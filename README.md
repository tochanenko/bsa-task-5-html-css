# BSA - Task 5 - HTML, CSS

### List of My Improvements

* Translucent header (looks so nice!)
* Custom Font
* Custom mobile version for browsers with width less then 600px
* Translucent notifications
* There is a bunny! Can you find it?

### You can [check my homework on my site](https://binary.tochanenko.com/task5/) without even cloning!